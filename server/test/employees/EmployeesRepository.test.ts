import { EmployeesRepository } from '../../src/employees';

describe('EmployeeRepository', () => {
  let repo: EmployeesRepository;

  beforeEach(async () => {
    repo = new EmployeesRepository();
    await repo.createEmployees(10);
  });

  describe('constructor', () => {
    it('creates sever instance', () => {
      expect(repo).toBeInstanceOf(EmployeesRepository);
    });
  });

  describe('getEmployees', () => {
    it('returns employee list', async () => {
      const list = await repo.getEmployees();

      expect(list).toBeInstanceOf(Array);
      expect(list.length).toBeGreaterThan(0);
    });
  });

  describe('getEmployee', () => {
    it('returns employee list', async () => {
      const employees = await repo.getEmployees();
      const employee = employees[0];

      const res = await repo.getEmployee(employee.id);

      expect(res).toEqual(employee);
    });
  });

  describe('getEmployeesPairs', () => {
    it('returns employee pairs', async () => {
      await repo.createPairs();

      const pairs = await repo.getPairs();

      expect(pairs).toBeInstanceOf(Array);
      expect(pairs.length).toBeGreaterThan(0);
    });

    it('returns employee pairs that not match', async () => {
      await repo.createPairs();

      const pairs = await repo.getPairs();

      pairs.forEach(pair => {
        expect(pair.first.team).not.toEqual(pair.second.team);
        expect(pair.first.district).not.toEqual(pair.second.district);
        expect(pair.first.age).not.toEqual(pair.second.age);
        expect(pair.first.visionDefect).not.toEqual(pair.second.visionDefect);
      });
    });
  });

  describe('deleteEmployees', () => {
    it('clear employee list', async () => {
      await repo.deleteEmployees();

      const list = await repo.getEmployees();
      expect(list).toEqual([]);
    });
  });
});
