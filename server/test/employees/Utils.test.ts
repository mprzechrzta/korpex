import { generateRandomEmployee, drawNumber } from '../../src/employees/Utils';

describe('Utils', () => {
  describe('generateRandomEmployee', () => {
    it('creates random employee', () => {
      const employee = generateRandomEmployee();

      expect(employee).toHaveProperty('id');
      expect(employee).toHaveProperty('name');
      expect(employee).toHaveProperty('team');
      expect(employee).toHaveProperty('district');
      expect(employee).toHaveProperty('age');
      expect(employee).toHaveProperty('visionDefect');
    });
  });

  describe('drawNumber', () => {
    it('return random number from range', () => {
      const min = 25;
      const max = 50;

      const res = drawNumber(max, min);

      expect(res).toBeGreaterThanOrEqual(min);
      expect(res).toBeLessThanOrEqual(max);
    });
  });
});
