import { EmployeesHandler, EmployeesRepository } from '../../src/employees';
import { Request, Response } from 'express';

jest.mock('../../src/employees/EmployeesRepository');

describe('EmployeeHandler', () => {
  let repo: EmployeesRepository;
  let handler: EmployeesHandler;
  let req: Request;
  let res: Response;

  beforeEach(() => {
    repo = new EmployeesRepository();
    handler = new EmployeesHandler(repo);
    req = {} as Request;
    res = {} as Response;
    res.send = jest.fn().mockReturnValue({ status: jest.fn() });
    res.setHeader = jest.fn();
  });

  describe('constructor', () => {
    it('creates sever instance', () => {
      expect(handler).toBeInstanceOf(EmployeesHandler);
    });
  });

  describe('handleCreateEmployees', () => {
    it('sends employee count', async () => {
      const count = 10;
      req.body = { count };
      repo.createEmployees = jest.fn().mockResolvedValue(undefined);

      await handler.handleCreateEmployees(req, res);

      expect(res.send).toHaveBeenCalledWith({ count });
    });
  });

  describe('handleGetEmployee', () => {
    it('sends employee', async () => {
      const employee = { name: 'Jhon Smith' };
      req.params = { id: '0' };
      repo.getEmployee = jest.fn().mockResolvedValue(employee);

      await handler.handleGetEmployee(req, res);

      expect(res.send).toHaveBeenCalledWith(employee);
    });

    it('sends not found', async () => {
      req.params = { id: '0' };
      repo.getEmployee = jest.fn().mockResolvedValue(undefined);

      await handler.handleGetEmployee(req, res);

      expect(res.send).toHaveBeenCalledWith('Not Found');
    });
  });

  describe('handleGetEmployees', () => {
    it('sends employee list', async () => {
      const employeeList = [{ name: 'Jhon Smith' }];
      repo.getEmployees = jest.fn().mockResolvedValue(employeeList);

      await handler.handleGetEmployees(req, res);

      expect(res.send).toHaveBeenCalledWith(employeeList);
    });
  });

  describe('handleGetPairs', () => {
    it('sends employee pairs', async () => {
      const pairs = [
        {
          first: { name: 'Jhon Smith' },
          second: { name: 'Bob Builder' }
        }
      ];
      repo.getPairs = jest.fn().mockResolvedValue(pairs);

      await handler.handleGetPairs(req, res);

      expect(res.send).toHaveBeenCalledWith(pairs);
    });
  });

  describe('handleCreatePairs', () => {
    it('sends create pairs status', async () => {
      const status = true;
      repo.createPairs = jest.fn().mockResolvedValue(status);

      await handler.handleCreatePairs(req, res);

      expect(res.send).toHaveBeenCalledWith({ status });
    });
  });
});
