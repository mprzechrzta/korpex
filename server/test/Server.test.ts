import { Server } from '../src/server';
import * as Express from 'express';
import { EmployeesHandler, EmployeesRepository } from '../src/employees';

jest.mock('express', () => {
  return jest.fn().mockImplementation(() => {
    return {
      use: jest.fn(),
      get: jest.fn(),
      post: jest.fn(),
      listen: jest.fn()
    };
  });
});
jest.mock('../src/employees/EmployeesHandler');
jest.mock('../src/employees/EmployeesRepository');

describe('Server', () => {
  let app: Express.Application;
  let repo: EmployeesRepository;
  let handler: EmployeesHandler;
  let server: Server;

  beforeEach(() => {
    // @ts-ignore
    Express.static = jest.fn();
    app = Express();
    repo = new EmployeesRepository();
    handler = new EmployeesHandler(repo);
    server = new Server(app, handler);
  });

  describe('constructor', () => {
    it('creates sever instance', () => {
      expect(server).toBeInstanceOf(Server);
    });

    it('sets up routes', () => {
      expect(app.get).toBeCalledWith('/employees', expect.any(Function));
      expect(app.post).toBeCalledWith('/employees', expect.any(Function));
      expect(app.get).toBeCalledWith('/employees/:id', expect.any(Function));
      expect(app.get).toBeCalledWith('/pairs', expect.any(Function));
      expect(app.post).toBeCalledWith('/pairs', expect.any(Function));
    });
  });

  describe('start', () => {
    it('sets up server to listen on right port', () => {
      const cb = () => undefined;

      server.start(3000, cb);

      expect(app.listen).toBeCalledWith(3000, cb);
    });
  });
});
