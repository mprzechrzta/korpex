# korpex-service

## Provides REST API for korpex system

### Install dependencies

```cmd
npm install
```

### Start service for development. It uses nodemone and ts-node to recompile on fly without formating and linting

```cmd
npm run start-dev
```

### Build project. Formats, lints and builds code

```cmd
npm run build
```

### Test project. Runs unit tests and coverage. 100% coverage is required

```cmd
npm test
```
