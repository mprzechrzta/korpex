import { Server } from './server';
import * as Express from 'express';
import { EmployeesHandler, EmployeesRepository } from './employees';

const port = Number(process.env.PORT) || 3000;
const app = Express();
const repo = new EmployeesRepository();
const handler = new EmployeesHandler(repo);
const server = new Server(app, handler);
server.start(port, () => console.log(`Server is running! Visit korpex page at http://localhost:${port}`));
