import * as bodyParser from 'body-parser';
import { Application } from 'express';
import { EmployeesHandler } from './employees';
import * as Express from 'express';
import * as Path from 'path';
import * as Cors from 'cors';

export class Server {
  constructor(private app: Application, handler: EmployeesHandler) {
    app.use(Cors());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    const path = Path.join(__dirname, '../../client/dist');
    app.use('/', Express.static(path));

    app.get('/employees', handler.handleGetEmployees.bind(handler));
    app.post('/employees', handler.handleCreateEmployees.bind(handler));
    app.get('/employees/:id', handler.handleGetEmployee.bind(handler));
    app.get('/pairs', handler.handleGetPairs.bind(handler));
    app.post('/pairs', handler.handleCreatePairs.bind(handler));
  }

  public start(port: number, cb?: () => void): void {
    this.app.listen(port, cb);
  }
}
