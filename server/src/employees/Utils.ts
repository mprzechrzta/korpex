import * as Uuid from 'uuid';
import { Employee } from './EmployeeTypes';
// tslint:disable-next-line
const Fakerator = require('fakerator');

const teams = ['testers', 'developers', 'dev-ops', 'office', 'finance'];
const districts = [
  'muchobor',
  'krzyki',
  'borek',
  'gaj',
  'brochów',
  'biskupin',
  'popowice'
];

export const generateRandomEmployee = (): Employee => {
  const fakerator = new Fakerator();
  return {
    id: Uuid.v4(),
    name: fakerator.names.nameM(),
    avatar: `https://randomuser.me/api/portraits/men/${drawNumber(100)}.jpg`,
    team: teams[drawNumber(teams.length)],
    district: districts[drawNumber(districts.length)],
    visionDefect: drawNumber(2) === 1 ? true : false,
    age: drawNumber(50, 25)
  };
};

export const drawNumber = (max: number, min: number = 0): number => {
  const range = max - min;
  const result = Math.floor(Math.random() * range);
  return result + min;
};
