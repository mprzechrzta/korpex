import { EmployeesPair, Employee } from './EmployeeTypes';
import { generateRandomEmployee } from './Utils';

export class EmployeesRepository {
  private pairs: EmployeesPair[] = [];
  private employees: Employee[] = [];

  public async getEmployees(): Promise<Employee[]> {
    return this.employees;
  }

  public async getEmployee(id: string): Promise<Employee | undefined> {
    return this.employees.find(e => e.id === id);
  }

  public async createEmployees(count: number): Promise<void> {
    for (let i = 0; i < count; i++) {
      const employee = generateRandomEmployee();
      this.employees.push(employee);
    }
  }

  public async deleteEmployees(): Promise<void> {
    this.employees = [];
  }

  public async getPairs(): Promise<EmployeesPair[]> {
    return this.pairs;
  }

  public async createPairs(): Promise<boolean> {
    this.pairs = [];

    const createPair = (employees: Employee[]) => {
      const employee = employees.pop();
      if (!employee) {
        return;
      }

      const notMatch = employees.find(e => {
        return (
          employee.team !== e.team &&
          employee.district !== e.district &&
          employee.age !== e.age &&
          employee.visionDefect !== e.visionDefect
        );
      });

      if (notMatch) {
        this.pairs.push({
          first: employee,
          second: notMatch
        });

        const restOfEmployees = employees.filter(e => e.id !== notMatch.id);

        createPair(restOfEmployees);
      } else {
        createPair(employees);
      }
    };

    createPair([...this.employees]);

    return this.pairs.length > 0;
  }
}
