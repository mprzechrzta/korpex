import { Request, Response } from 'express';
import { EmployeesRepository } from './EmployeesRepository';

export class EmployeesHandler {
  constructor(private repo: EmployeesRepository) {}

  public async handleGetEmployees(req: Request, res: Response): Promise<void> {
    const employees = await this.repo.getEmployees();
    res.send(employees);
  }

  public async handleCreateEmployees(
    req: Request,
    res: Response
  ): Promise<void> {
    const count = req.body.count;
    this.repo.deleteEmployees();
    await this.repo.createEmployees(count);
    res.send({ count }).status(201);
  }

  public async handleGetEmployee(req: Request, res: Response): Promise<void> {
    const id = req.params.id;
    const employee = await this.repo.getEmployee(id);
    if (employee) {
      res.send(employee);
    } else {
      res.send('Not Found').status(404);
    }
  }

  public async handleGetPairs(req: Request, res: Response): Promise<void> {
    const pairs = await this.repo.getPairs();
    res.send(pairs);
  }

  public async handleCreatePairs(req: Request, res: Response): Promise<void> {
    const status = await this.repo.createPairs();
    res.send({ status }).status(201);
  }
}
