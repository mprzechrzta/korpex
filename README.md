# Korpex app

Application consists of server and UI client.

Server provides REST API and implements required endpoints for korpex UI client.

To run the app you must have docker service running.
Go to project directory and issue the following command.

```cmd
docker-compose up
```

Then open browser with url http://localhost:3000
