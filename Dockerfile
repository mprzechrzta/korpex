FROM node:10

WORKDIR /app/server
COPY ./server/package*.json ./
RUN npm install
COPY ./server .
RUN npm run build

WORKDIR /app/client
COPY ./client/package*.json ./
RUN npm install
COPY ./client .
RUN npm run build

WORKDIR /app/server
EXPOSE 3000
CMD [ "node", "build/index.js" ]
