import { EmployeesPair, Employee } from './EmployeesTypes';

export class FetchService {
  private readonly url = 'http://localhost:3000';

  public async createEmployees(count: number = 100): Promise<EmployeesPair[]> {
    return (
      await fetch(`${this.url}/employees`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ count })
      })
    ).json();
  }

  public async getEmployee(id: string): Promise<Employee> {
    return (await fetch(`${this.url}/employees/${id}`)).json();
  }

  public async getPairs(): Promise<EmployeesPair[]> {
    return (await fetch(`${this.url}/pairs`)).json();
  }

  public async createPairs(): Promise<{ status: boolean }> {
    return (
      await fetch(`${this.url}/pairs`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        }
      })
    ).json();
  }
}
