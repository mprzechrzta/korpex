import React, { Component } from 'react';
import styles from './Styles.scss';
import { FetchService } from './FetchService';
import { Button, Grid } from '@material-ui/core';
import { EmployeesPair, Employee } from './EmployeesTypes';
import { EmployeeCard } from './EmployeeCard';
import { EmployeesPairs } from './EmployeesPairs';
import { LoadingStatus } from './LoadingStatus';
import korpexPng from './korpex.png';

type State = {
  pairs: EmployeesPair[];
  employeesCreated: boolean;
  pairsCreated: boolean;
  drawing: boolean;
  creating: boolean;
  first: Employee | null;
  second: Employee | null;
};

type Props = {
  fetchService: FetchService;
};

class EmployeesApp extends Component<Props, State> {
  constructor(props: any) {
    super(props);
    this.state = {
      pairs: [],
      employeesCreated: false,
      pairsCreated: false,
      drawing: false,
      creating: false,
      first: null,
      second: null
    };
  }

  async generatePairs() {
    this.setState({ drawing: true });
    const res = await this.props.fetchService.createPairs();
    const pairs = await this.props.fetchService.getPairs();
    this.setState({ pairs, drawing: false, pairsCreated: res.status });
    if (pairs.length > 0) {
      await this.selectPair(0);
    }
  }

  async createEmployees() {
    this.setState({ creating: true });
    await this.props.fetchService.createEmployees();
    this.setState({ employeesCreated: true, creating: false });
  }

  async selectPair(idx: number) {
    const pair = this.state.pairs[idx];
    const first = await this.props.fetchService.getEmployee(pair.first.id);
    const second = await this.props.fetchService.getEmployee(pair.second.id);
    this.setState({ first, second });
  }

  render() {
    return (
      <React.Fragment>
        <div className={styles.Logo}>
          <img src={korpexPng} />
        </div>
        <Grid container spacing={3} justify="center" alignItems="center">
          <Grid item xs={6}>
            <Button
              className={styles.Button}
              onClick={() => this.createEmployees()}
              variant="contained"
              color="primary"
            >
              Generate Example Employees
            </Button>
          </Grid>
          <Grid item xs={6}>
            <LoadingStatus
              loading={this.state.creating}
              success={this.state.employeesCreated}
            />
          </Grid>
          <Grid item xs={6}>
            <Button
              className={styles.Button}
              onClick={() => this.generatePairs()}
              variant="contained"
              color="primary"
            >
              Draw Employees Pairs
            </Button>
          </Grid>
          <Grid item xs={6}>
            <LoadingStatus
              loading={this.state.drawing}
              success={this.state.pairsCreated}
            />
          </Grid>
          <Grid item xs={6}>
            <EmployeesPairs
              onSelect={idx => this.selectPair(idx)}
              pairs={this.state.pairs}
            />
          </Grid>
          <Grid item xs={3}>
            <EmployeeCard user={this.state.first} />
          </Grid>
          <Grid item xs={3}>
            <EmployeeCard user={this.state.second} />
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}

export default EmployeesApp;
