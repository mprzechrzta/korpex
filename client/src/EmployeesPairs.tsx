import React from 'react';
import styles from './Styles.scss';
import AvatarGroup from '@material-ui/lab/AvatarGroup';
import {
  Avatar,
  List,
  ListItem,
  ListItemText,
} from '@material-ui/core';
import { EmployeesPair, Employee } from './EmployeesTypes';

type Props = {
  pairs: EmployeesPair[];
  onSelect: (idx: number) => void;
};

export const EmployeesPairs = (props: Props) => {
  const onSelect = props.onSelect;
  const pairs = props.pairs;

  if (pairs.length === 0) {
    return <p>Before drawing first you need to generate some employees!</p>;
  }

  return (
    <List className={styles.EmployeesPairs}>
      {pairs.map((p, i) => (
        <ListItem
          onClick={() => onSelect(i)}
          className={styles.PairItem}
          key={p.first.id + p.second.id}
          button
        >
          <AvatarGroup>
            <Avatar alt={p.first.name} src={p.first.avatar} />
            <Avatar alt={p.second.name} src={p.second.avatar} />
          </AvatarGroup>
          <ListItemText
            className={styles.ListItem}
            primary={`${p.first.name} / ${p.second.name}`}
          />
        </ListItem>
      ))}
    </List>
  );
};
