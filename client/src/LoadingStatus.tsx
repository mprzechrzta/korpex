import React from 'react';
import { CircularProgress } from '@material-ui/core';
import { Check } from '@material-ui/icons';

type Props = {
  loading: boolean;
  success: boolean;
};

export const LoadingStatus = (props: Props) => {
  if (props.loading) {
    return <CircularProgress color="secondary" />;
  }

  if (props.success && !props.loading) {
    return <Check style={{ color: 'green', fontSize: 40 }} />;
  }

  return null;
};
