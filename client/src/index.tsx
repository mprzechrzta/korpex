import React from "react";
import ReactDOM from "react-dom";
import EmployeesApp from "./EmployeesApp";
import { FetchService } from "./FetchService";

const appDiv = document.createElement("div");
document.body.appendChild(appDiv);

ReactDOM.render(<EmployeesApp fetchService={new FetchService()} />, appDiv);
