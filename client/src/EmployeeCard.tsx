import React from 'react';
import styles from './Styles.scss';
import {
  Avatar,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Card
} from '@material-ui/core';
import { Employee } from './EmployeesTypes';

type Props = {
  user: Employee | null;
};

export const EmployeeCard = (props: Props) => {
  const user = props.user;

  if (!user) {
    return null;
  }

  return (
    <Card className={styles.Card}>
      <List>
        <ListItem>
          <ListItemAvatar>
            <Avatar
              alt={user.name}
              className={styles.Avatar}
              src={user.avatar}
            />
          </ListItemAvatar>
          <ListItemText primary={user.name} />
        </ListItem>
        <List>
          <ListItem>
            <ListItemText secondary={`team: ${user.team}`} />
          </ListItem>
          <ListItem>
            <ListItemText secondary={`district: ${user.district}`} />
          </ListItem>
          <ListItem>
            <ListItemText secondary={`age: ${user.age}`} />
          </ListItem>
          <ListItem>
            <ListItemText secondary={`visionDefect: ${user.visionDefect}`} />
          </ListItem>
        </List>
      </List>
    </Card>
  );
};
