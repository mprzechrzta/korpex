export interface Employee {
  id: string;
  name: string;
  avatar: string;
  team: string;
  age: number;
  district: string;
  visionDefect: boolean;
}

export interface EmployeesPair {
  first: Employee;
  second: Employee;
}
